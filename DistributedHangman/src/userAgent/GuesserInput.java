package userAgent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;

public class GuesserInput implements Callable<MasterMessage> {

	private Communicator com;
	private ArrayBlockingQueue<MasterMessage> queue;
	private BufferedReader reader;
	private PrintWriter writer;
	
	public GuesserInput(Communicator com, ArrayBlockingQueue<MasterMessage> queue, BufferedReader reader, PrintWriter writer) {
		this.com = com;
		this.queue = queue;
		this.reader = reader;
		this.writer = writer;
	}

	public MasterMessage call() {
		String s = null;
		MasterMessage masterMessage = null;
		GuesserMessage guesserMessage = null;
		Character move = null;
		boolean firstTurn = true;
		boolean receivedAck = true;
		try {
			while(true) {
				
				if (!firstTurn) {
					receivedAck = false;
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
					}
					while (!(receivedAck || queue.isEmpty())) {
						masterMessage = queue.poll();
						if (masterMessage.getTriedChars().contains(move.toString()))
							receivedAck = true;
					}
				} else {
					writer.println("Attesa della selezione della parola");
					writer.flush();
					firstTurn = false;
				}
				if (receivedAck)
					s = reader.readLine();
	        	if (s == null) {
	        		System.err.println("NULL READ");
	        		break;
	        	} else if (s.matches("[a-zA-Z]")) {
	        		move = new Character(Character.toUpperCase(s.charAt(0)));
	        		guesserMessage = new GuesserMessage(move);
	        		com.send(guesserMessage);
	        	} else if (s.equalsIgnoreCase("exit")) {
	        		writer.println("Uscita dalla partita");
	        		writer.flush();
	        		break;
	        	} else {
	        		writer.println("Comando sconosciuto: \'EXIT\' per abbandonare la partita");
	        		writer.flush();
	        		s = move.toString();
	        	}
			}
		} catch (IOException e) {
			if(!e.getMessage().equals("Stream closed"))
				e.printStackTrace();
		}
		return null;
	}
	
}
