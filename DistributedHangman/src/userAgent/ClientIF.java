package userAgent;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientIF extends Remote {
	
	public void printInfos(String infos) throws RemoteException;
	
}
