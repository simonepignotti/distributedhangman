package userAgent;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Master extends Thread {

	private Communicator com;
	private ArrayBlockingQueue<Message> queue;
	private BufferedReader reader;
	private PrintWriter writer;

	private HashSet<Character> alreadyTried;
	private WordToGuess wtg;
	private int counter;
	private boolean endGame;

	public Master(Communicator com) {
		super("master");
		this.com = com;
		queue = new ArrayBlockingQueue<Message>(50);
		alreadyTried = new HashSet<Character>(10);
		wtg = null;
		counter = 0;
		endGame = false;
	}

	public void run() {

		Console console = System.console();
		reader = new BufferedReader(console.reader());
		writer = console.writer();
		
		chooseWord();

		ExecutorService executor = Executors.newFixedThreadPool(2);
		// thread che riceve i messaggi dei guesser
		Receiver receiver = new Receiver(com, queue, writer, true);
		// thread che gestisce l'input da tastiera
		MasterInput inHandler = new MasterInput(reader, writer);

		Future<Object> recFut = executor.submit(receiver);
		Future<Object> inFut = executor.submit(inHandler);

		GuesserMessage guesserMessage = null;
		MasterMessage masterMessage = new MasterMessage(wtg.getGuessedPart(),
				alreadyTried, counter, endGame);

		writer.println(masterMessage.toString());
		writer.flush();

		boolean sent = false;
		int i = 0;

		// PRIMO MESSAGGIO
		while (!sent && i < 10) {
			try {
				com.send(masterMessage);
				sent = true;
			} catch (IOException e) {
				e.printStackTrace();
				i++;
			}
		}
		if (!sent) {
			writer.println("Errore durante la comunicazione. Chiusura della partita in corso...");
			writer.flush();
			this.interrupt();
		}

		// LOOP DELLA PARTITA
		while (!(inFut.isDone() || recFut.isDone() || endGame || this
				.isInterrupted())) {
			// attesa di una lettera dai guesser
			while (queue.isEmpty() && !inFut.isDone() && !this.isInterrupted()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					this.interrupt();
				}
			}
			if (!queue.isEmpty() && !inFut.isDone() && !this.isInterrupted()) {
				guesserMessage = (GuesserMessage) queue.poll();

				writer.println(guesserMessage.toString());
				writer.flush();

				Character move = guesserMessage.getMove();
				if (!alreadyTried.contains(move)) {
					if (!wtg.guess(move))
						counter++;
					alreadyTried.add(move);
				}
				endGame = (wtg.isGuessed() || (counter == Client.CHANCES));
				masterMessage = new MasterMessage(wtg.getGuessedPart(),
						alreadyTried, counter, endGame);

				writer.println(masterMessage.toString());
				writer.flush();

				sent = false;
				i = 0;
				while (!sent && i < 10) {
					try {
						com.send(masterMessage);
						sent = true;
					} catch (IOException e) {
						e.printStackTrace();
						i++;
					}
				}
				if (!sent) {
					writer.println("Errore durante la comunicazione. Chiusura della partita in corso...");
					writer.flush();
					this.interrupt();
				}
			}
		}

		if (endGame) {
			if (wtg.isGuessed()) {
				writer.println("La parola è stata indovinata");
				writer.println("SCONFITTA");
				writer.flush();
			} else if (counter >= Client.CHANCES) {
				writer.println("Possibilità esaurite");
				writer.println("VITTORIA");
				writer.flush();
			}
		}
		
		// nel caso di chiusura manuale della partita vengono avvisati i guesser
		if (inFut.isDone()) {
			masterMessage = new MasterMessage(wtg.getGuessedPart(),
					alreadyTried, counter, true);
			try {
				com.send(masterMessage);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			com.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		synchronized (this) {
			notify();
		}
	}

	private void chooseWord() {
		String temp = "";
		while (true) {
			writer.print("Parola da indovinare: ");
			writer.flush();
			try {
				temp = reader.readLine();
			} catch (IOException e) {
				writer.println("Error: " + e.getMessage());
				writer.flush();
				e.printStackTrace();
				System.exit(1);
			}
			if (temp.matches("[a-zA-Z]{3,10}")) {
				wtg = new WordToGuess(temp);
				break;
			} else {
				writer.println("Inserire una parola composta da almeno 3 caratteri e fino a 10 (senza spazi o accenti)");
				writer.flush();
			}
		}
	}

}
