package userAgent;

import org.json.simple.JSONObject;

public class Message {
	
	protected boolean isFromGuesser;
	protected boolean isFromMaster;
	
	protected JSONObject obj;
	
	public Message () {
		obj = null;
		isFromGuesser = false;
		isFromMaster = false;
	}
	
	public Message (JSONObject obj) {
		this.obj = obj;
		isFromGuesser = false;
		isFromMaster = false;
	}
	
	public JSONObject toJSON() {
		return obj;
	}
	
	public boolean isFromGuesser() {
		return isFromGuesser;
	}
	
	public boolean isFromMaster() {
		return isFromMaster;
	}
	
	public static Message sort (Message m) {
		JSONObject obj = m.toJSON();
		if (obj.containsKey(MasterMessage.WORD))
			return new MasterMessage(obj);
		else if (obj.containsKey(GuesserMessage.MOVE))
			return new GuesserMessage(obj);
		else
			return m;
	}
}
