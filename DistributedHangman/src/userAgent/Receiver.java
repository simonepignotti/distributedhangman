package userAgent;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;

import org.json.simple.parser.ParseException;

public class Receiver implements Callable<Object> {

	private Communicator com;
	private ArrayBlockingQueue<Message> queue;
	private PrintWriter writer;
	private boolean master;

	public Receiver(Communicator com, ArrayBlockingQueue<Message> queue, PrintWriter writer, boolean master) {
		this.com = com;
		this.queue = queue;
		this.writer = writer;
		this.master = master;
	}

	public Object call() {
		Message m;
		boolean received;
		int i;
		while (true) {
			try {
				m = null;
				received = false;
				i = 0;
				while (!received && i < 50) {
					try {
						m = com.receive();
						m = Message.sort(m);
						received = master ? m.isFromGuesser() : m.isFromMaster();
					} catch (ParseException e) {
					}
					i++;
				}
				if (i == 50) {
					writer.println("Errore durante la comunicazione. Chiusura della partita in corso...");
					writer.flush();
					return null;
				}
			} catch (IOException e) {
				if (!e.getMessage().equals("Socket closed"))
					e.printStackTrace();
				return null;
			}

			boolean put = false;
			while (!put) {
				try {
					queue.put(m);
					put = true;
				} catch (InterruptedException e) {
				}
			}
		}
	}
	
}
