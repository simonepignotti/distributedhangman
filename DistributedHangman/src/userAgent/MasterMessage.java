package userAgent;

import java.util.ArrayList;
import java.util.HashSet;

import org.json.simple.*;

public class MasterMessage extends Message {

	public static final String WORD = "word";
	public static final String TRIED = "tried";
	public static final String COUNTER = "counter";
	public static final String ENDGAME = "endgame";
	
	private String alreadyGuessed;
	private ArrayList<String> alreadyTried;
	private int counter;
	private boolean endGame;
	
	@SuppressWarnings("unchecked")
	public MasterMessage (JSONObject obj) {
		isFromMaster = true;
		isFromGuesser = false;
		alreadyGuessed = (String) obj.get(WORD);
		alreadyTried = (ArrayList<String>) obj.get(TRIED);
		counter = Integer.valueOf((String) obj.get(COUNTER)).intValue();
		endGame = (boolean) obj.get(ENDGAME);
	}
	
	public MasterMessage (String alreadyGuessed, HashSet<Character> alreadyTried, int counter, boolean endGame) {
		isFromMaster = true;
		isFromGuesser = false;
		this.alreadyGuessed = alreadyGuessed;
		this.alreadyTried = new ArrayList<String>();
		for (Character c : alreadyTried)
			this.alreadyTried.add(c.toString());
		this.counter = counter;
		this.endGame = endGame;
	}
	
	public String getWord() {
		return alreadyGuessed;
	}
	
	public String getTriedChars() {
		return alreadyTried.toString();
	}
	
	public int getCounter() {
		return counter;
	}
	
	public boolean hasBeenTried(Character c) {
		return alreadyTried.contains(c.toString());
	}
	
	public boolean endOfGame() {
		return endGame;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put(WORD, alreadyGuessed);
		obj.put(TRIED, alreadyTried);
		obj.put(COUNTER, new Integer(counter).toString());
		obj.put(ENDGAME, endGame);
		return obj;
	}

	public String toString() {
		String message = "";
		if (endGame) {
			if (alreadyGuessed.contains("_")) {
				if (counter < Client.CHANCES)
					message += "Il master ha abbandonato la partita\n";
			} else
				message += "Partita terminata! La parola era " + alreadyGuessed + "\n";
		} else {
			message += "|-----------------------------|\n";
			message += "|" + alreadyGuessed + "\n";
			message += "|" + (Client.CHANCES - counter) + " possibilita' rimaste\n";
			message += "|" + (alreadyTried.isEmpty() ? "Nessun tentativo effettuato" : alreadyTried.toString()) + "\n";
			message += stickman(counter);
			message += "|-----------------------------|";
		}
		return message;
	}
	
	public static String stickman (int counter) {
		String sm = "";
		sm += "|   ___\n";
		sm += "|  |   |\n";
		sm += "|  |   "+((counter>0)?"O\n":"\n");
		sm += "|  |  "+((counter>2)?"/":" ")+((counter>1)?"|":"")+((counter>3)?"\\\n":"\n");
		sm += "|  |  "+((counter>4)?"/ ":" ")+((counter>5)?"\\\n":"\n");
		sm += "| _|_\n";
		return sm;
	}
}
