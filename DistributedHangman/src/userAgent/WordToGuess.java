package userAgent;

import java.util.ArrayList;

public class WordToGuess {

	private String wtg, alreadyGuessed;
	private ArrayList<Character> charsToGuess;
	
	public WordToGuess (String s) {
		wtg = s.toUpperCase();
		alreadyGuessed = "";
		charsToGuess = new ArrayList<Character>();
		for (char c : wtg.toCharArray()) {
			charsToGuess.add(new Character(c));
			alreadyGuessed += "_ ";
		}
	}
	
	public boolean guess (Character move) {
		boolean found = false;
		while (charsToGuess.remove(move))
			found = true;
		for (int i = 0; i < wtg.length(); i++)
			if (wtg.charAt(i) == move.charValue())
				alreadyGuessed = alreadyGuessed.substring(0, 2*i) + wtg.charAt(i) + alreadyGuessed.substring(2*i+1);
		return found;
	}
	
	public String getGuessedPart() {
		return alreadyGuessed;
	}
	
	public boolean isGuessed() {
		return !alreadyGuessed.contains("_");
	}
	
}
