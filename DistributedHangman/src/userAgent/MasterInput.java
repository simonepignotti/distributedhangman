package userAgent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Callable;

public class MasterInput implements Callable<Object> {
	
	private BufferedReader reader;
	private PrintWriter writer;
	
	public MasterInput(BufferedReader reader, PrintWriter writer) {
		this.reader = reader;
		this.writer = writer;
	}

	public Object call() {
		try {
			while (true) {
				if (reader.readLine().equalsIgnoreCase("exit")) {
					writer.println("Uscita dalla partita");
					writer.flush();
					break;
				} else {
					writer.println("Comando sconosciuto: \'EXIT\' per abbandonare la partita");
					writer.flush();
				}
			}
		} catch (IOException e) {
			if(!e.getMessage().equals("Stream closed"))
				e.printStackTrace();
		}
		return null;
	}
	
}
