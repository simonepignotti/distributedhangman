package userAgent;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import registry.ServerIF;
import registry.TCPHandler;

public class Client extends UnicastRemoteObject implements ClientIF {

	private static final long serialVersionUID = 1L;

	public static int CHANCES = 6;
	private static int TIMEOUT = 3600000;

	private static final String CONF_FILE = "conf.json";
	private static final String SERVER = "server";
	private static final String TCP_PORT = "tcp_port";
	private static final String INTERFACE = "interface";
	private static final String HOSTNAME = "hostname";
	private static final String RMI_PORT = "rmi_port";
	private static final String RMI_NAME = "rmi_name";

	private static BufferedReader reader;
	private static PrintWriter writer;

	private static String username;
	private static String password;

	public Client() throws RemoteException {
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {

		ServerIF rmiServer = null;
		String serverAddr = null;
		String hostname = null;
		String rmiName = null;
		InetAddress iface = null;
		InetAddress group = null;
		int udpPort = 0;
		int tcpPort = 0;
		int rmiPort = 0;
		String key = "";
		Communicator com = null;
		Thread playThread = null;

		Console console = System.console();
		reader = new BufferedReader(console.reader());
		writer = console.writer();

		// lettura JSON di configurazione
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(CONF_FILE));
			String conf = new String(encoded, Charset.defaultCharset()).trim();
			JSONObject confJson = (JSONObject) new JSONParser().parse(conf);
			serverAddr = (String) confJson.get(SERVER);
			tcpPort = Integer.valueOf((String) confJson.get(TCP_PORT));
			iface = InetAddress.getByName((String) confJson.get(INTERFACE));
			hostname = (String) confJson.get(HOSTNAME);
			rmiName = (String) confJson.get(RMI_NAME);
			rmiPort = Integer.valueOf((String) confJson.get(RMI_PORT));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
			writer.println("Errore nel caricamento del file di configurazione: "
					+ e.getMessage());
			writer.flush();
			System.exit(1);
		}

		// collegamento alla registry
		System.setProperty("java.rmi.server.hostname", hostname);
		try {
			rmiServer = (ServerIF) Naming.lookup("rmi://" + serverAddr + ":"
					+ rmiPort + "/" + rmiName);
		} catch (NotBoundException e) {
			e.printStackTrace();
			writer.println("Errore nella connessione RMI: " + e.getMessage());
			writer.flush();
			System.exit(1);
		}

		writer.print("Gia' registrato? (Y/N): ");
		writer.flush();
		String temp = reader.readLine();
		boolean registered = temp.equalsIgnoreCase("Y") ? true : false;
		if (registered) {
			writer.println("LOGIN");
			writer.flush();
		} else {
			writer.println("CREAZIONE NUOVO ACCOUNT");
			writer.flush();
		}

		boolean signedIn = false;
		while (!signedIn) {
			writer.print("Username: ");
			writer.flush();
			username = reader.readLine();
			writer.print("Password: ");
			writer.flush();
			password = new String(console.readPassword());
			signedIn = registered ? rmiServer.logIn(new Client(), username,
					password) : rmiServer.register(new Client(), username,
					password);
			if (!signedIn) {
				if (registered) {
					writer.println("Credenziali errate!");
					writer.flush();
				} else {
					writer.println("Errore durante la registrazione, provare di nuovo");
					writer.flush();
				}
			}
		}

		Socket tcpSocket = null;
		PrintWriter socketOut = null;
		BufferedReader socketIn = null;

		while (true) {

			JSONObject obj = new JSONObject();
			boolean decided = false;
			boolean isMaster = false;
			boolean exit = false;
			while (!decided) {
				writer.println("Comandi: master <N> ; guesser <MASTER> ; exit");
				temp = reader.readLine();
				if (temp.matches("master [0-9]+")) {
					int players = Integer.valueOf(temp.substring(7));
					obj.put(TCPHandler.MASTER_USERNAME, username);
					obj.put(TCPHandler.NO_OF_GUESSERS, players);
					decided = true;
					isMaster = true;
				} else if (temp.matches("guesser [a-zA-Z0-9]+")) {
					String masterUsername = temp.substring(8);
					obj.put(TCPHandler.MASTER_USERNAME, masterUsername);
					decided = true;
					isMaster = false;
				} else if (temp.equalsIgnoreCase("exit")) {
					decided = true;
					exit = true;
				}
			}

			if (exit)
				break;

			try {
				tcpSocket = new Socket(serverAddr, tcpPort);
				socketOut = new PrintWriter(tcpSocket.getOutputStream(), true);
				socketIn = new BufferedReader(new InputStreamReader(
						tcpSocket.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
				writer.println("Errore durante la comunicazione con il server");
				writer.flush();
				System.exit(1);
			}

			socketOut.println(obj.toString());
			
			writer.println("In attesa di altri giocatori");
			writer.flush();
			
			temp = socketIn.readLine();

			socketIn.close();
			socketOut.close();

			tcpSocket.close();

			JSONObject gameDetails = null;
			try {
				gameDetails = (JSONObject) new JSONParser().parse(temp);
			} catch (ParseException e) {
				e.printStackTrace();
				System.exit(1);
			}
			
			boolean full = false;
			
			if ((boolean) gameDetails.get(TCPHandler.JOINED)) {
				group = InetAddress.getByName((String) gameDetails
						.get(TCPHandler.GROUP));
				udpPort = ((Long) gameDetails.get(TCPHandler.PORT)).intValue();
				key = (String) gameDetails.get(TCPHandler.KEY);
			} else {
				writer.println("La partita selezionata ha raggiunto il numero massimo di giocatori");
				writer.flush();
				full = true;
			}
			
			if (!full) {
			
				try {
					com = new Communicator(iface, group, udpPort, key);
				} catch (IOException e) {
					writer.println("Errore durante il setup della rete: "
							+ e.getMessage());
					writer.flush();
					System.exit(1);
				}
	
				playThread = isMaster ? new Master(com) : new Guesser(com);
				playThread.start();
	
				try {
					synchronized (playThread) {
						playThread.wait(TIMEOUT);
					}
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (playThread.isAlive()) {
						writer.println("Partita chiusa per scadenza del tempo a disposizione");
						playThread.interrupt();
						synchronized (playThread) {
							playThread.wait(1000);
						}
						if (playThread.isAlive()) {
							writer.println("Errore: rilanciare il client");
							writer.flush();
							System.exit(1);
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	
				try {
					com.close();
				} catch (IOException e) {
					if (!(e.getMessage().equals("Socket is closed")))
						e.printStackTrace();
				}
			}
		}

		rmiServer.logOut(username);

		writer.println("BYE");
		writer.flush();

		reader.close();
		writer.close();
		
		System.exit(0);

	}

	public void printInfos(String infos) throws RemoteException {
		writer.println(infos);
		writer.flush();
	}

}
