package userAgent;

import org.json.simple.JSONObject;

public class GuesserMessage extends Message {

	public static final String MOVE = "move";
	
	private Character move;
	
	public GuesserMessage (JSONObject obj) {
		isFromMaster = false;
		isFromGuesser = true;
		move = new Character(((String) obj.get(MOVE)).charAt(0));
	}
	
	public GuesserMessage (Character move) {
		isFromMaster = false;
		isFromGuesser = true;
		this.move = move;
	}
	
	public Character getMove() {
		return move;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put(MOVE, move.toString());
		return obj;
	}
	
	public String toString() {
		return ("Lettera richiesta: " + move);
	}
	
}
