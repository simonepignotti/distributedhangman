package userAgent;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Guesser extends Thread {
	
	private Communicator com;
	private ArrayBlockingQueue<Message> readingQueue;
	private ArrayBlockingQueue<MasterMessage> ackQueue;
	private BufferedReader reader;
	private PrintWriter writer;
	
	public Guesser(Communicator com) {
		super("guesser");
		this.com = com;
		readingQueue = new ArrayBlockingQueue<Message>(50);
		ackQueue = new ArrayBlockingQueue<MasterMessage>(50);
	}
	
	public void run() {
		
		Console console = System.console();
		reader = new BufferedReader(console.reader());
		writer = console.writer();
		
		ExecutorService executor = Executors.newFixedThreadPool(2);
		// thread che riceve i messaggi del master
		Receiver receiver = new Receiver(com, readingQueue, writer, false);
		// thread che gestisce l'input da tastiera
		GuesserInput inHandler = new GuesserInput(com, ackQueue, reader, writer);

		Future<Object> recFut = executor.submit(receiver);
		Future<MasterMessage> inFut = executor.submit(inHandler);
		
		MasterMessage masterMessage = null;
		boolean endGame = false;
		while (!(inFut.isDone() || recFut.isDone() || endGame || this.isInterrupted())) {
			// attesa di un messaggio nella coda
			while (readingQueue.isEmpty() && !inFut.isDone() && !this.isInterrupted()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					this.interrupt();
				}
			}
			// elaborazione del messaggio del master
			if (!readingQueue.isEmpty() && !inFut.isDone() && !this.isInterrupted()) {
				masterMessage = (MasterMessage) readingQueue.poll();
				
				writer.println(masterMessage.toString());
				writer.flush();
				
				if (masterMessage.endOfGame())
					endGame = true;
				
				boolean put = false;
				while (!put) {
					try {
						ackQueue.put(masterMessage);
						put = true;
					} catch (InterruptedException e) {
					}
				}
			}
		}
		
		if (endGame) {
			if (!masterMessage.getWord().contains("_")) {
				writer.println("La parola e' stata indovinata");
				writer.println("VITTORIA");
				writer.flush();
			}
			else if (masterMessage.getCounter() >= Client.CHANCES) {
				writer.println("Possibilita' esaurite");
				writer.println(MasterMessage.stickman(Client.CHANCES));
				writer.println("SCONFITTA");
				writer.flush();
			}
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			com.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		synchronized (this) {
			notify();	
		}
		
		writer.println("guesser exiting");
		writer.flush();
		
	}
	
}
