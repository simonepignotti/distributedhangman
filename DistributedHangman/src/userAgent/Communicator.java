package userAgent;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.jasypt.util.text.BasicTextEncryptor;

public class Communicator {

	private MulticastSocket socket;
	private InetAddress group;
	private int port;
	private BasicTextEncryptor crypt = new BasicTextEncryptor();
	
	public Communicator(InetAddress iface, InetAddress group, int port, String key) throws IOException {
		this.group = group;
		this.port = port;
		crypt.setPassword(key);
		socket = new MulticastSocket(port);
		socket.setInterface(iface);
		socket.joinGroup(group);
	}
	
	public void send(Message m) throws IOException {
		byte[] buf = crypt.encrypt(m.toJSON().toString()).getBytes();
		socket.send(new DatagramPacket(buf, buf.length, group, port));
	}
	
	public Message receive() throws IOException, ParseException {
		byte[] buf = new byte[512];
		DatagramPacket p = new DatagramPacket(buf, buf.length);
		socket.receive(p);
		JSONObject obj = (JSONObject) new JSONParser().parse(crypt.decrypt(new String(p.getData()).trim()));
		return new Message(obj);
	}
	
	public void close() throws IOException {
		socket.leaveGroup(group);
		socket.close();
	}
	
}
