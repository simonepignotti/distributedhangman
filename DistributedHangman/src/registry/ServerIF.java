package registry;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerIF extends Remote {
	
	public boolean register(Object callback, String username, String password) throws RemoteException;
	
	public boolean logIn(Object callback, String username, String password) throws RemoteException;
	
	public boolean logOut(String username) throws RemoteException;

}
