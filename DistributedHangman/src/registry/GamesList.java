package registry;

import java.util.ArrayList;

public class GamesList {

	public ArrayList<Game> gamesList;

	public GamesList() {
		gamesList = new ArrayList<Game>(Server.maxGames);
	}

	public boolean remove(Game game) {
		return gamesList.remove(game);
	}

	public synchronized Game add(String username, int players) {
		if (gamesList.size() < Server.maxGames) {
			Game ng = new Game(username, players);
			gamesList.add(ng);
			return ng;
		} else {
			return null;
		}
	}

	public synchronized Game get(String master) {
		int i = 0;
		while (i < gamesList.size()) {
			if (gamesList.get(i).getMaster().equals(master))
				break;
			i++;
		}
		if (i < gamesList.size())
			return gamesList.get(i);
		else
			return null;
	}

	public String toString() {
		String infos = "|---------------Partite Disponibili-----------------|\n";
		if (gamesList.isEmpty()) {
			infos += "|\n| Nessuna partita disponibile\n";
		} else {
			infos += "|\n| Master\tNum giocatori\tGruppo\n|\n";
			for (Game game : gamesList) {
				infos += "| " + game.toString() + "\n";
			}
		}
		infos += "|\n|---------------------------------------------------|";
		return infos;
	}

}
