package registry;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import userAgent.ClientIF;

public class Server extends UnicastRemoteObject implements ServerIF {

	private static final long serialVersionUID = 1L;

	private static final String CONF_FILE = "conf.json";
	private static final String HOSTNAME = "hostname";
	private static final String MAX_GAMES = "max_games";
	private static final String RMI_PORT = "rmi_port";
	private static final String RMI_NAME = "rmi_name";
	private static final String TCP_PORT = "tcp_port";
	
	// users e passwords
	private static ConcurrentHashMap<String, String> passwords;
	// users e interfacce RMI corrispondenti
	private static ConcurrentHashMap<String, ClientIF> rmiClients;

	protected static GamesList gamesList;
	protected static String hostname = null;
	protected static String rmiName = null;
	protected static int rmiPort = 0;
	protected static int tcpPort = 0;
	protected static int maxGames = 0;

	public Server() throws RemoteException {
	}

	public static void main(String[] args) throws RemoteException {

		passwords = new ConcurrentHashMap<String, String>();
		rmiClients = new ConcurrentHashMap<String, ClientIF>();
		gamesList = new GamesList();

		// lettura del JSON di configurazione
		try {
			byte[] encoded1 = Files.readAllBytes(Paths.get(CONF_FILE));
			String conf = new String(encoded1, Charset.defaultCharset()).trim();
			JSONObject confJson = (JSONObject) new JSONParser().parse(conf);
			hostname = (String) confJson.get(HOSTNAME);
			rmiName = (String) confJson.get(RMI_NAME);
			rmiPort = Integer.valueOf((String) confJson.get(RMI_PORT));
			tcpPort = Integer.valueOf((String) confJson.get(TCP_PORT));
			maxGames = Integer.valueOf((String) confJson.get(MAX_GAMES));
		} catch (IOException | ParseException e) {
			System.err
					.println("Errore nel caricamento del file di configurazione: "
							+ e.getMessage());
			System.exit(1);
		}
		
		// Avvio servizio RMI
		System.setProperty("java.rmi.server.hostname", hostname);
		Server RMIServer = new Server();
		Registry reg = LocateRegistry.createRegistry(rmiPort);
		reg.rebind(rmiName, RMIServer);
		System.out.println("Server Ready");

		ServerSocket tcpSocket = null;

		try {
			tcpSocket = new ServerSocket(tcpPort);
		} catch (IOException e) {
			System.err.println("Could not listen on port " + tcpPort);
			System.exit(1);
		}

		// accettazione connesioni TCP per avvio partite
		
		Executor threadPool = Executors.newCachedThreadPool();

		try {
			while (true) {
				threadPool.execute(new TCPHandler(tcpSocket.accept()));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				tcpSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// chiama la printInfos su tutti i client che hanno effettuato l'accesso
	public static void sendInfos() {
		String infos = gamesList.toString();
		for (String user : rmiClients.keySet()) {
			try {
				rmiClients.get(user).printInfos(infos);
			} catch (RemoteException e) {
				rmiClients.remove(user);
			}
		}
	}

	public boolean register(Object callBack, String username, String password)
			throws RemoteException {
		if (passwords.containsKey(username))
			return false;
		else {
			Server.passwords.put(username, password);
			System.err.println(username + " registered with password "
					+ passwords.get(username));
			Server.rmiClients.put(username, (ClientIF) callBack);
			((ClientIF)callBack).printInfos(gamesList.toString());
			return true;
		}
	}

	public boolean logIn(Object callBack, String username, String password)
			throws RemoteException {
		if (passwords.get(username).equals(password)) {
			System.err.println(username + " logged in");
			Server.rmiClients.put(username, (ClientIF) callBack);
			((ClientIF)callBack).printInfos(gamesList.toString());
			return true;
		} else {
			return false;
		}
	}

	public boolean logOut(String username) throws RemoteException {
		Server.rmiClients.remove(username);
		return true;
	}
	
}
