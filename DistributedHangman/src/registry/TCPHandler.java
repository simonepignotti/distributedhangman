package registry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class TCPHandler extends Thread {

	public static final String NO_OF_GUESSERS = "no_of_guessers";
	public static final String MASTER_USERNAME = "master_username";

	public static final String JOINED = "joined";
	public static final String GROUP = "group";
	public static final String PORT = "port";
	public static final String KEY = "key";

	private Socket socket = null;

	public TCPHandler(Socket socket) {
		this.socket = socket;
	}

	@SuppressWarnings("unchecked")
	public void run() {

		PrintWriter out;
		BufferedReader in;

		try {
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		try {
			Game game = null;
			JSONObject gameDetails = new JSONObject();

			String inputLine = in.readLine();

			JSONObject obj = (JSONObject) new JSONParser().parse(inputLine);
			// richiesta di creazione partita (master)
			if (obj.containsKey(NO_OF_GUESSERS)) {
				String username = (String) obj.get(MASTER_USERNAME);
				int players = ((Long) obj.get(NO_OF_GUESSERS)).intValue();
				game = Server.gamesList.add(username, players);
				Server.sendInfos();
			// richiesta join partita esistente (guesser)
			} else if (obj.containsKey(MASTER_USERNAME)) {
				String master = (String) obj.get(MASTER_USERNAME);
				game = Server.gamesList.get(master);
				if (game != null)
					if (!game.join())
						game = null;
			} else {
				throw new ParseException(0);
			}
			if (game != null) {
				gameDetails.put(GROUP, game.getGroup());
				gameDetails.put(PORT, game.getPort());
				gameDetails.put(KEY, game.getKey());
				while (!game.isReady()) {
					try {
						sleep(100);
					} catch (InterruptedException e) {
					}
				}
				Server.gamesList.remove(game);
			}
			gameDetails.put(JOINED, (game != null));
			out.println(gameDetails.toString());
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		} finally {
			out.close();
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
