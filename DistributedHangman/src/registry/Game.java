package registry;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Game {

	private static int ip0 = 224;
	private static int ip1 = 0;
	private static int ip2 = 0;
	private static int ip3 = 0;
	
	private String master;
	private int players;
	private String group;
	private int port;
	private String key;
	
	private int actualPlayers;
	private boolean ready;
	
	public Game (String master, int players) {
		this.master = master;
		this.players = players;
		key = new BigInteger(130, new SecureRandom()).toString(32);
		actualPlayers = 0;
		ready = false;
		group = ip0 + "." + ip1 + "." + ip2 + "." + ip3;
		updateIP();
		port = 4698;
	}
	
	public String getGroup() {
		return group;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getMaster() {
		return master;
	}
	
	public int getNoOfPlayers() {
		return players;
	}
	
	public String getKey() {
		return key;
	}
	
	public boolean isReady() {
		return ready;
	}
	
	public synchronized boolean join() {
		if (!ready) {
			actualPlayers++;
			if (actualPlayers == players)
				ready = true;
			return true;
		} else {
			return false;
		}
	}
	
	public String toString() {
		String infos = "";
		infos += master + "\t";
		infos += players + "\t\t";
		infos += group;
		return infos;
	}
	
	public boolean equals(Object o) {
		Game g = (Game) o;
		if (g.getMaster().equals(master))
			return true;
		else
			return false;
	}
	
	// Aggiorna l'IP da usare per la prossima partita
	
	private void updateIP() {
		if (ip3 == 255) {
			ip3 = 0;
			if (ip2 == 255) {
				ip2 = 0;
				if (ip1 == 255) {
					ip1 = 0;
					if (ip0 == 239) {
						ip0 = 224;
					} else {
						ip0++;
					}
				} else {
					ip1++;
				}
			} else {
				ip2++;
			}
		} else {
			ip3++;
		}
	}
	
}
